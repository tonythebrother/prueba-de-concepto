<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Practica de contexto</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
        <script src="{{ asset(mix('js/app.js')) }}"></script>
       
    </head>
    <body class="antialiased">
        <header id="header"></header>
        <main>
            <aside id="order-list"></aside>
            <section id="monitoring"></section>
        </main>
        <script src="js/app.js"></script>
    </body>
</html>
