
var data = [
    {
        "id": "1",
        "order": "5431",
        "status": "Listo",
        "customer": "Andrés",
        "delivery": "Gerard",
        "address": "Paseo Benavides, 0, 7º",
        "commerce": "Supermercado El Oasis"
    },
    {
        "id": "2",
        "order": "5432",
        "status": "Preparando",
        "customer": "Lionel",
        "delivery": "Andrés",
        "address": "Plaza Martí, 79, 3º B",
        "commerce": "Cien Acres"
    },
    {
        "id": "3",
        "order": "5433",
        "status": "En Camino",
        "customer": "Gerard",
        "delivery": "Andrés",
        "address": "Praza Marc, 25, 7º B",
        "commerce": "Smart Market"
    },
    {
        "id": "4",
        "order": "5434",
        "status": "Preparando",
        "customer": "Eva",
        "delivery": "Andrés",
        "address": "Carrer Salinas, 089, 9º A",
        "commerce": "Mis Raíces"
    },
    {
        "id": "5",
        "order": "5435",
        "status": "Orden recibida",
        "customer": "Hugo",
        "delivery": "Gerard",
        "address": "Avinguda Mateo, 019, 7º C",
        "commerce": "Supermercado El Terrenal"
    },
    {
        "id": "6",
        "order": "5436",
        "status": "Preparando",
        "customer": "Juan",
        "delivery": "Lionel",
        "address": "Avinguda Iria, 260, 63º A",
        "commerce": "La Cesta De Oro"
    },
    {
        "id": "7",
        "order": "5437",
        "status": "En Camino",
        "customer": "Álvaro",
        "delivery": "Lionel",
        "address": "Avinguda Sánchez, 3, 3º F",
        "commerce": "La Gran Despensa"
    },
    {
        "id": "8",
        "order": "5438",
        "status": "Orden recibida",
        "customer": "Leo",
        "delivery": "Gerard",
        "address": "Calle Pacheco, 32, 5º E",
        "commerce": "Maxi Despensa"
    },
    {
        "id": "9",
        "order": "5439",
        "status": "Listo",
        "customer": "Luz",
        "delivery": "Lionel",
        "address": "Praza Carbajal, 154, 1º C",
        "commerce": "La Pradera Minimarket"
    },
    {
        "id": "10",
        "order": "5440",
        "status": "Cancelado",
        "customer": "Sara",
        "delivery": "Lionel",
        "address": "Plaça Emma, 7, 8º C",
        "commerce": "Supermercado El Origen"
    },
    {
        "id": "11",
        "order": "5441",
        "status": "En Camino",
        "customer": "Ulises",
        "delivery": "Andrés",
        "address": "Praza Ruvalcaba, 36, 5º",
        "commerce": "Super Mercado El Rural"
    },
    {
        "id": "12",
        "order": "5442",
        "status": "Preparando",
        "customer": "Héctor",
        "delivery": "Gerard",
        "address": "Calle Carranza, 775, 1º F",
        "commerce": "La Gran Canasta"
    }

]

export default data;