import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import data from './Data';


function OrderList() {

    const [orders, setOrders] = useState([]);

    useEffect(() => setOrders(data), []);

    return (
        <nav className="orders bg-light">
           
            <ul class="nav nav-pills mb-3 list-group" id="pills-tab" role="tablist">

            <li className="list-group-item">
                    <h6>NÚMERO DE ORDEN</h6>
                </li>

                <li className="nav-item list-group-item-action ">
                    <a className="nav-link bg-primary text-light general" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="false">
                        Estatus general
                    </a>
                </li>

                {!orders ? 'Loading...' :
                    orders.map((order) => {

                        return (
                            <li key={order.id} className=" list-group-item-action">
                                <a className="nav-link text-dark" id={"pills-"+order.id+"-tab"} data-toggle="pill" href={"#pills-"+order.id} role="tab" aria-controls={"pills-"+order.id} aria-selected="false">
                                    #{order.order}
                                </a>
                                <p className="nav-text bg-secondary text-light">
                                    {order.status}
                                </p>
                            </li>
                        );

                    })}

            </ul>
        </nav>
    );
}

export default OrderList;

if (document.getElementById('order-list')) {
    ReactDOM.render(<OrderList />, document.getElementById('order-list'));
}
