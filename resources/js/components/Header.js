import React from 'react';
import ReactDOM from 'react-dom';

function Header() {

    let date = new Date();

    let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };

    const getDate = () => {

        const d = date.toLocaleDateString('es-MX', options).toString();

        const hour0 = date.getHours(); 
        let hour1 = (hour0 > 11)? hour0 - 12 : hour0;
        hour1 = (hour0 == -12)? hour1 * -1 : hour1;

        const minutes0 = date.getMinutes();
        let minutes1 = (minutes0 < 10)? "0"+minutes0.toString() : minutes0.toString();
        minutes1 = (hour0 > 11)?  minutes1 + " P.M." : minutes1 + " A.M.";

        const today = d.charAt(0).toUpperCase() + d.slice(1) + ", " + hour1 + ":" + minutes1; 

        return today;

    }

    return (
        <nav className="navbar shadow-sm text-light">
            <h3 className="font-weight-bold">
                NOMBRE DE LA EMPRESA
            </h3>
            <h5>
                {getDate()}
            </h5>
        </nav>
    );
}

export default Header;

if (document.getElementById('header')) {
    ReactDOM.render(<Header />, document.getElementById('header'));
}