import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import data from './Data';

function getOrders() {

    const [orders, setOrders] = useState([]);

    useEffect(() => getData(), []);

    const getData = () => {

        setOrders(data);

    }

    return orders;

}

function Monitoring() {

    const orders = getOrders();

    var general_status = {
        total: orders.length,
        recived: 0,
        preparing: 0,
        ready: 0,
        transporting: 0,
        canceled: 0
    }

    const getData = () => {

        for (const o in orders) {

            if (orders[o].status === "Preparando") ++general_status.preparing;
            if (orders[o].status === "Listo") ++general_status.ready;
            if (orders[o].status === "En Camino") ++general_status.transporting;
            if (orders[o].status === "Cancelado") {
                ++general_status.canceled;
                --general_status.total;
            }
            if (orders[o].status === "Orden recibida") ++general_status.recived;

        }

    }

    return (
        <article className="bg-light">

            <div className="tab-content" id="pills-tabContent">

                <div className="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">

                    <h5>Estatus de ordenes general por día</h5>

                    <ul className="nav flex-column">
                        {getData()}
                        <li className="nav-item">
                            Total de Ordenes: {general_status.total}
                        </li>
                        <li className="nav-item">
                            Ordenes Recibidas: {general_status.recived}
                        </li>
                        <li className="nav-item">
                            Preparando: {general_status.preparing}
                        </li>
                        <li className="nav-item">
                            Listas: {general_status.ready}
                        </li>
                        <li className="nav-item">
                            En Camino: {general_status.transporting}
                        </li>
                        <li className="nav-item">
                            Canceladas: {general_status.canceled}
                        </li>
                    </ul>

                </div>

                {orders.map(o => {

                    return (
                        <div key={o.id} className="tab-pane fade" id={"pills-"+o.id} role="tabpanel" aria-labelledby={"pills-"+o.id+"-tab"}>
                            <ul className="nav flex-column">

                                <li className="nav-item">
                                    Número de orden: {o.order}
                                </li>
                                <li className="nav-item">
                                    Cliente: {o.customer}
                                </li>
                                <li className="nav-item">
                                    Estatus: {o.status}
                                </li>
                                <li className="nav-item">
                                    Delivery: {o.delivery}
                                </li>
                                <li className="nav-item">
                                    Dirección: {o.address}
                                </li>
                                <li className="nav-item">
                                    Comercio: {o.commerce}
                                </li>
                            </ul>
                        </div>
                    );

                })}

            </div>

            <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=santo%20domingo&t=&z=13&ie=UTF8&iwloc=&output=embed" ></iframe>

        </article>
    );
}

export default Monitoring;

if (document.getElementById('monitoring')) {
    ReactDOM.render(<Monitoring />, document.getElementById('monitoring'));
}
