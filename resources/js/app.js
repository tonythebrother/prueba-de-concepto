require('./bootstrap');

import 'bootstrap/dist/js/bootstrap.bundle.min';

require('./components/Header');
require('./components/OrderList');
require('./components/Monitoring');
